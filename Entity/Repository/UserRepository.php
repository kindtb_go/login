<?php

namespace Entity\Repository;

use Entity\User;

class UserRepository {
    const DB_HOST = "127.0.0.1";  //"localhost"
    const DB_USERNAME = "kindtb";
    const DB_PASSWORD = "";
    const DB_NAME = "pokemon";

    const TABLE_NAME = "user";

    const COLUMN_ID = "id";
    const COLUMN_FIRST_NAME = "first_name";
    const COLUMN_LAST_NAME = "last_name";
    const COLUMN_EMAIL = "email";
    const COLUMN_PASSWORD = "password";
    const COLUMN_ROLES = "roles";

    private $dbConn;

    public function __construct() {
        $this->dbConn = new mysqli(self::DB_HOST, self::DB_USERNAME, self::DB_PASSWORD, self::DB_NAME);;
    }

    public function findAll() {
        $sql = "SELECT * FROM ".self::TABLE_NAME;
        $result = $this->dbConn->query($sql);

        $users = array();
        while ($row = $result->fetch_assoc()) {
            $users[] = $this->createObject($row);
        }
        return $users;
    }

    /*
     * $row = array(
     *      "id" => 5,
     *      "first_name" => "admin",
     *      "last_name" => "",
     *      "email" => "admin@gmail.com",
     *      "password" => "xehjknbrhjvezhfbddrylbnkjrv",
     *      "roles" => "ROLE_USER"
     * );
     */
    private function createObject($row) {
        $user = new User();
        $user->setId($row[self::COLUMN_ID]);
        $user->setFirstName($row[self::COLUMN_FIRST_NAME]);
        $user->setLastName($row[self::COLUMN_LAST_NAME]);
        $user->setEmail($row[self::COLUMN_EMAIL]);
        $user->setPassword($row[self::COLUMN_PASSWORD]);

        $roles = explode(";",$row[self::COLUMN_ROLES]);
        foreach ($roles as $role) $user->addRole($role);

        return $user;
    }

}

