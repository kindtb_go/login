<?php
namespace Entity\Repository;

use Entity\User;

class UserRepositoryOld {

    const FILENAME = "data/users.csv";

    /**
     * @return array
     */
    public function findAll() {
        $lines = $this->loadLines();
        $users = array();
        foreach ($lines as $line) {
            $users[] = $this->createObject($line);
        }
        return $users;
    }

    /**
     * @param int $id
     * @return null|User
     */
    public function find($id) {
        $users = $this->findAll();
        /** @var User $user */
        foreach ($users as $user) {
            if ($user->getId() == $id) {
                return $user;
            }
        }
        return null;
    }

    public function findByEmail($email) {
        $users = $this->findAll();
        /** @var User $user */
        foreach ($users as $user) {
            if ($user->getEmail() == $email) {
                return $user;
            }
        }
        return null;
    }

    /**
     * @param User $newUser
     */
    public function add($newUser) {
        $users = $this->findAll();

        $highestId = 0;
        foreach ($users as $user) {
            if ($user->getId() > $highestId) {
                $highestId = $user->getId();
            }
        }

        $newUser->setId($highestId+1);
        $users[] = $newUser;

        $lines = array();
        foreach ($users as $user) {
            $lines[] = $this->createLine($user);
        }
        $this->saveLines($lines);
    }

    public function save($changedUser) {
        if (!$changedUser->getId()) {
            $this->add($changedUser);
            return;
        }

        $users = $this->findAll();

        foreach ($users as $index => $user) {
            if ($user->getId() == $changedUser->getId()) {
                $users[$index] = $changedUser;
            }
        }

        $lines = array();
        foreach ($users as $user) {
            $lines[] = $this->createLine($user);
        }
        $this->saveLines($lines);
    }

    /**
     * @return array
     */
    private function loadLines() {
        $content = file_get_contents(self::FILENAME);
        $lines = explode("\n",$content);
        $validLines = [];
        foreach ($lines as $line) {
            if ($line != "") {
                $validLines[] = $line;
            }
        }
        return $validLines;
    }

    /**
     * @param array $lines
     */
    private function saveLines($lines) {
        file_put_contents(self::FILENAME,implode("\n",$lines));
    }

    /**
     * @param string $line
     * @return User
     */
    private function createObject($line) {
        $data = explode(",",$line);
        $user = new User();
        $user->setId($data["0"]);
        $user->setFirstName($data["1"]);
        $user->setLastName($data["2"]);
        $user->setEmail($data["3"]);
        $user->setPassword($data["4"]);

        $roles = explode(";",$data["5"]);
        foreach ($roles as $role) $user->addRole($role);

        return $user;
    }

    /**
     * @param User $user
     * @return string
     */
    private function createLine($user) {
        $data = array(
            $user->getId(),
            $user->getFirstName(),
            $user->getLastName(),
            $user->getEmail(),
            $user->getPassword(),
            implode(";",$user->getRoles())
        );
        return implode(",",$data);
    }
} 