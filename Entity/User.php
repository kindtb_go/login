<?php
namespace Entity;

use Model\Role\RoleManager;

class User {

    /** @var int */
    private $id;

    /** @var string */
    private $firstName;

    /** @var string */
    private $lastName;

    /** @var string */
    private $email;

    /** @var string */
    private $password;

    /** @var array */
    private $roles = array(RoleManager::ROLE_USER);

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getFirstName() {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     */
    public function setFirstName($firstName) {
        $this->firstName = $firstName;
    }

    /**
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return array
     */
    public function getRoles() {
        return $this->roles;
    }

    /**
     * @param string $role
     * @return bool
     */
    public function hasRole($role)
    {
        foreach($this->roles as $userRole) {
            if ($userRole == $role)
                return true;
        }
        return false;
    }

    /**
     * @param string $role
     */
    public function addRole($role)
    {
        if (!$this->hasRole($role))
            $this->roles[] = $role;
    }

    /**
     * @param string $role
     */
    public function removeRole($role) {
        foreach ($this->roles as $index=>$userRole) {
            if ($userRole == $role) {
                unset($this->roles[$index]);
                return;
            }
        }
    }
} 