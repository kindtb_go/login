<?php
namespace Model\Form;
use Entity\User;

class PasswordForm {
    const FORM_ID = "id";
    const FORM_EMAIL = "email";
    const FORM_PASSWORD = "password";
    const FORM_SUBMIT = "Save Password";

    /** @var User */
    private $user;
    private $post;

    public function __construct($user) {
        $this->user = $user;
    }

    public function getValue($formElement) {
        switch ($formElement) {
            case self::FORM_ID:
                return $this->user->getId();
            case self::FORM_EMAIL:
                return $this->user->getEmail();
            case self::FORM_SUBMIT:
                return "Save Password";
        }
        return "";
    }

    public function validatePostData($post) {
        $this->post = $post;
        return
            isset($post[self::FORM_ID]) &&
            isset($post[self::FORM_PASSWORD]);
    }

    public function getPassword() {
        return $this->post[self::FORM_PASSWORD];
    }
} 