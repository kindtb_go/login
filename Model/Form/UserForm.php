<?php
namespace Model\Form;

class UserForm {
    const FORM_ID = "id";
    const FORM_FIRST_NAME = "first_name";
    const FORM_LAST_NAME = "last_name";
    const FORM_EMAIL = "email";
    const FORM_SUBMIT = "submit";

    /** @var User */
    private $user;
    private $post;

    public function __construct($user) {
        $this->user = $user;
    }

    public function getValue($formElement) {
        switch ($formElement) {
            case self::FORM_ID:
                return $this->user->getId();
            case self::FORM_FIRST_NAME:
                return $this->user->getFirstName();
            case self::FORM_LAST_NAME:
                return $this->user->getLastName();
            case self::FORM_EMAIL:
                return $this->user->getEmail();
            case self::FORM_SUBMIT:
                return $this->user->getId() ? "Save User" : "Create User";
        }
        return "";
    }

    public function validatePostData($post) {
        $this->post = $post;
        return
            isset($post[self::FORM_ID]) &&
            isset($post[self::FORM_FIRST_NAME]) &&
            isset($post[self::FORM_LAST_NAME]) &&
            isset($post[self::FORM_EMAIL]);
    }

    public function getNewData() {
        $this->user->setFirstName($this->post[self::FORM_FIRST_NAME]);
        $this->user->setLastName($this->post[self::FORM_LAST_NAME]);
        $this->user->setEmail($this->post[self::FORM_EMAIL]);
        return $this->user;
    }
} 