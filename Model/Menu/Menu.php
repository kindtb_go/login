<?php
namespace Model\Menu;

use Entity\User;
use Model\Role\RoleManager;

class Menu {
    const MENU_ITEM_HOME = "Homepage";
    const MENU_ITEM_PAGE_1 = "Page 1";
    const MENU_ITEM_PAGE_2 = "Page 2";
    const MENU_ITEM_PAGE_3 = "Page 3";
    const MENU_ITEM_USERS = "Users";
    const MENU_ITEM_LOGIN = "Login";
    const MENU_ITEM_LOGOUT = "Logout";

    /** @var User */
    private $user;

    private $itemsLeft = array();
    private $itemsRight = array();

    public function __construct($user) {
        $this->user = $user;
        $this->itemsLeft[] = new MenuItem(self::MENU_ITEM_HOME  , "index.php", null);
        $this->itemsLeft[] = new MenuItem(self::MENU_ITEM_PAGE_1, "page1.php", RoleManager::ROLE_USER);
        $this->itemsLeft[] = new MenuItem(self::MENU_ITEM_PAGE_2, "page2.php", RoleManager::ROLE_USER);
        $this->itemsLeft[] = new MenuItem(self::MENU_ITEM_PAGE_3, "page3.php", RoleManager::ROLE_EXPERT);
        $this->itemsLeft[] = new MenuItem(self::MENU_ITEM_USERS , "users.php", RoleManager::ROLE_ADMIN);

        if ($user) {
            $this->itemsRight[] = new MenuItem(self::MENU_ITEM_LOGOUT,"logout.php", null);
            $this->itemsRight[] = new MenuItem($this->user->getFirstName(),null, null);
        } else {
            $this->itemsRight[] = new MenuItem(self::MENU_ITEM_LOGIN,"login.php", null);
        }

    }

    public function activate($pageName) {
        /** @var MenuItem $menuItem */
        foreach ($this->itemsLeft as $menuItem) {
            $menuItem->setActive($menuItem->getName() == $pageName);
        }
        foreach ($this->itemsRight as $menuItem) {
            $menuItem->setActive($menuItem->getName() == $pageName);
        }
    }

    public function getItemsLeft() {
        return $this->filterItems($this->itemsLeft);
    }

    public function getItemsRight() {
        return $this->filterItems($this->itemsRight);
    }

    private function filterItems($allItems) {
        $items = array();
        /** @var MenuItem $item */
        foreach ($allItems as $item) {
            if (is_null($item->getRequiredRole())) {
                $items[] = $item;
            } else {
                if ($this->user && $this->user->hasRole($item->getRequiredRole())) {
                    $items[] = $item;
                }
            }
        }
        return $items;
    }

} 