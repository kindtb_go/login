<?php
namespace Model\Menu;

class MenuItem {

    private $name;
    private $url;
    private $requiredRole = null;
    private $active = false;

    public function __construct($name, $url, $requiredRole) {
        $this->name = $name;
        $this->url = $url;
        $this->requiredRole = $requiredRole;
    }

    public function getName() {
        return $this->name;
    }

    public function getUrl() {
        return $this->url;
    }

    public function isActive() {
        return $this->active;
    }

    public function setActive($active) {
        $this->active = $active;
    }

    public function getRequiredRole() {
        return $this->requiredRole;
    }

} 