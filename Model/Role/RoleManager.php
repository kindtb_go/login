<?php

namespace Model\Role;

class RoleManager {
    const ROLE_USER = "ROLE_USER";
    const ROLE_EXPERT = "ROLE_EXPERT";
    const ROLE_ADMIN = "ROLE_ADMIN";

    public function getRoles() {
        return array(
            self::ROLE_USER,
            self::ROLE_EXPERT,
            self::ROLE_ADMIN
        );
    }

} 