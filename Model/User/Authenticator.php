<?php
/**
 * Created by PhpStorm.
 * User: bart
 * Date: 12.10.17
 * Time: 20:18
 */

namespace Model\User;

use Entity\Repository\UserRepository;


class Authenticator {

    public function authenticate($email, $password) {
        $passwordEncoder = new PasswordEncoder();
        $encodedPassword = $passwordEncoder->encode($password);
        $userRepository = new UserRepository();
        $user = $userRepository->findByEmail($email);
        if ($user) {
            return $user->getPassword() == $encodedPassword ? $user : null;
        }
        return null;
    }
} 