<?php
include('include/autoloader.php');
use Model\Menu\Menu;

session_start();

$menu = new Menu(isset($_SESSION["user"]) ? $_SESSION["user"] : null);
$menu->activate(Menu::MENU_ITEM_HOME);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
    <div class="container">
        <?php include("views/header.php"); ?>

        <div class="jumbotron text-center">
            <h1>This is the homepage</h1>
            <p>Everyone can see this page</p>
        </div>
        <div class="jumbotron">
            <p>Try to log in as:</p>
            <ul>
                <li>a normal user: user/user</li>
                <li>an expert: expert/expert</li>
                <li>a site admin: admin/admin</li>
            </ul>
        </div>
    </div>
</body>
</html>