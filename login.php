<?php
include('include/autoloader.php');
use Model\Form\LoginForm;
use Model\User\Authenticator;

session_start();

$loginMessage = "";

$loginForm = new LoginForm();
if ($loginForm->validatePostData($_POST)) {
    $email = $loginForm->getEmail();
    $password = $loginForm->getPassword();
    $authenticator = new Authenticator();
    if ($user = $authenticator->authenticate($email,$password)) {
        $_SESSION["user"] = $user;
        header('location: index.php');
    } else {
        $loginMessage = "Incorrect email and password combination";
    }

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <form action="<?php echo $_SERVER['PHP_SELF']; ?>" method="POST">
        <div class="row">
            <div class="col-xs-6 text-right">
                Email
            </div>
            <div class="col-xs-6">
                <input type="text" name="<?php echo LoginForm::FORM_EMAIL; ?>" value="" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-right">
                Password
            </div>
            <div class="col-xs-6">
                <input type="password" name="<?php echo LoginForm::FORM_PASSWORD; ?>" value="" />
            </div>
        </div>
        <?php if ($loginMessage): ?>
            <div class="row">
                <div class="col-xs-offset-6 col-xs-6" style="color: red; font-weight: bold">
                    <?php echo $loginMessage; ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="row">
            <div class="col-xs-offset-6 col-xs-6">
                <input type="submit" value="Login" />
            </div>
        </div>
    </form>
    <a href="index.php">Cancel</a>
</div>
</body>
</html>