<?php
include('include/autoloader.php');
use Entity\User;
use Model\Menu\Menu;
use Model\Role\RoleManager;

session_start();

/** @var User $user */
$user = isset($_SESSION["user"]) ? $_SESSION["user"] : null;

if (!$user || !$user->hasRole(RoleManager::ROLE_USER)) {
    header('Location: index.php');
}

$menu = new Menu($user);
$menu->activate(Menu::MENU_ITEM_PAGE_1);

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <?php include("views/header.php"); ?>

    <div class="jumbotron text-center">
        <h1>This is page 1</h1>
        <p>Only registered users can access this page</p>
    </div>
</div>
</body>
</html>