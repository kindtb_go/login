<?php
include('include/autoloader.php');
use Entity\Repository\UserRepository;
use Entity\User;
use Model\Form\UserForm;
use Model\Menu\Menu;
use Model\Role\RoleManager;

session_start();

/** @var User $user */
$user = isset($_SESSION["user"]) ? $_SESSION["user"] : null;

if (!$user || !$user->hasRole(RoleManager::ROLE_ADMIN)) {
    header('Location: index.php');
}

$menu = new Menu($user);
$menu->activate(Menu::MENU_ITEM_USERS);

$userRepository = new UserRepository();
$userId = isset($_GET["id"]) ? $_GET["id"] : null;
$user = $userId ? $userRepository->find($userId) : new User();

$userForm = new UserForm($user);
if ($userForm->validatePostData($_POST)) {
    $user = $userForm->getNewData();
    $userRepository->save($user);
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <?php include("views/header.php"); ?>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $userId; ?>" method="POST">
        <div class="row">
            <div class="col-xs-6 text-right">
                First Name
            </div>
            <div class="col-xs-6">
                <input type="text" name="<?php echo UserForm::FORM_FIRST_NAME; ?>" value="<?php echo $userForm->getValue(UserForm::FORM_FIRST_NAME); ?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-right">
                Last Name
            </div>
            <div class="col-xs-6">
                <input type="text" name="<?php echo UserForm::FORM_LAST_NAME; ?>" value="<?php echo $userForm->getValue(UserForm::FORM_LAST_NAME); ?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-right">
                Email
            </div>
            <div class="col-xs-6">
                <input type="text" name="<?php echo UserForm::FORM_EMAIL; ?>" value="<?php echo $userForm->getValue(UserForm::FORM_EMAIL); ?>" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-offset-6 col-xs-6">
                <input type="hidden" name="<?php echo UserForm::FORM_ID; ?>" value="<?php echo $userForm->getValue(UserForm::FORM_ID); ?>" />
                <input type="submit" value="<?php echo $userForm->getValue(UserForm::FORM_SUBMIT); ?>" />
            </div>
        </div>
    </form>
</div>
</body>
</html>