<?php
include('include/autoloader.php');
use Entity\Repository\UserRepository;
use Entity\User;
use Model\Form\PasswordForm;
use Model\Menu\Menu;
use Model\Role\RoleManager;
use Model\User\PasswordEncoder;

session_start();

/** @var User $user */
$user = isset($_SESSION["user"]) ? $_SESSION["user"] : null;

if (!$user || !$user->hasRole(RoleManager::ROLE_ADMIN)) {
    header('Location: index.php');
}

$menu = new Menu($user);
$menu->activate(Menu::MENU_ITEM_USERS);

$userRepository = new UserRepository();
$userId = isset($_GET["id"]) ? $_GET["id"] : null;
$user = $userId ? $userRepository->find($userId) : new User();

$passwordForm = new PasswordForm($user);
if ($passwordForm->validatePostData($_POST)) {
    $password = $passwordForm->getPassword();
    $passwordEncoder = new PasswordEncoder();
    $user->setPassword($passwordEncoder->encode($password));
    $userRepository->save($user);
    header('location: users.php');
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <?php include("views/header.php"); ?>

    <form action="<?php echo $_SERVER['PHP_SELF']; ?>?id=<?php echo $userId; ?>" method="POST">
        <div class="row">
            <div class="col-xs-6 text-right">

            </div>
            <div class="col-xs-6">
                Enter the password for <?php echo $passwordForm->getValue(PasswordForm::FORM_EMAIL); ?>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-6 text-right">
                Password
            </div>
            <div class="col-xs-6">
                <input type="password" name="<?php echo PasswordForm::FORM_PASSWORD; ?>" value="" />
            </div>
        </div>
        <div class="row">
            <div class="col-xs-offset-6 col-xs-6">
                <input type="hidden" name="<?php echo PasswordForm::FORM_ID; ?>" value="<?php echo $passwordForm->getValue(PasswordForm::FORM_ID); ?>" />
                <input type="submit" value="<?php echo $passwordForm->getValue(PasswordForm::FORM_SUBMIT); ?>" />
            </div>
        </div>
    </form>
</div>
</body>
</html>