<?php
include('include/autoloader.php');
use Entity\User;
use Entity\Repository\UserRepository;
use Model\Menu\Menu;
use Model\Role\RoleManager;

session_start();

/** @var User $user */
$user = isset($_SESSION["user"]) ? $_SESSION["user"] : null;

if (!$user || !$user->hasRole(RoleManager::ROLE_ADMIN)) {
    header('Location: index.php');
}

$menu = new Menu($user);
$menu->activate(Menu::MENU_ITEM_USERS);

$userRepository = new UserRepository();
$users = $userRepository->findAll();
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <link rel="stylesheet" href="css/bootstrap.min.css">
</head>

<body>
<div class="container">
    <?php include("views/header.php"); ?>

    <table class="table">
        <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
        <?php foreach ($users as $user): ?>
            <tr>
                <td><?php echo $user->getFirstName(); ?></td>
                <td><?php echo $user->getLastName(); ?></td>
                <td><?php echo $user->getEmail(); ?></td>
                <td><a href="user_edit.php?id=<?php echo $user->getId(); ?>">edit</a></td>
                <td><a href="user_password.php?id=<?php echo $user->getId(); ?>">password</a></td>
                <td>roles</td>
            </tr>
        <?php endforeach; ?>

        </tbody>
    </table>
    <a class="btn btn-success" href="user_edit.php?id=0">New User</a>
</div>
</body>
</html>