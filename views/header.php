<nav class="navbar navbar-default">
    <div class="container-fluid">
        <ul class="nav navbar-nav">
            <?php
            foreach ($menu->getItemsLeft() as $menuItem) {
                include("menu_item.php");
            }
            ?>
        </ul>

        <ul class="nav navbar-nav nav navbar-right">
            <?php
            foreach ($menu->getItemsRight() as $menuItem) {
                include("menu_item.php");
            }
            ?>
        </ul>
    </div>
</nav>


<hr>