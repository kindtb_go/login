<li role="presentation" <?php if ($menuItem->isActive()) :?>class="active"<?php endif; ?>>
    <a href="<?php echo $menuItem->getUrl(); ?>">
        <?php echo $menuItem->getName(); ?>
    </a>
</li>